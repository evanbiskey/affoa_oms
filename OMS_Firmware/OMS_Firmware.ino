#include <bluefruit.h>

const int numReadings = 50;

int readings[numReadings];      // the readings from the analog input
int readIndex = 0;              // the index of the current reading
int total = 0;                  // the running total
int average = 0;                // the average

bool readRX = false;
bool buttonActive = false;
bool longPressActive = false;

#define debounce 20 // ms debounce period to prevent flickering when pressing or releasing the button
#define holdTime 2000 // ms hold period: how long to wait for press+hold event

// Button variables
int buttonVal = 0; // value read from button
int buttonLast = 0; // buffered value of the button's previous state
long btnDnTime; // time the button was pressed down
long btnUpTime; // time the button was released
boolean ignoreUp = false; // whether to ignore the button release because the click+hold was triggered
int buttonVal2 = 0; // value read from button
int buttonLast2 = 0; // buffered value of the button's previous state
long btnDnTime2; // time the button was pressed down
long btnUpTime2; // time the button was released
boolean ignoreUp2 = false; // whether to ignore the button release because the click+hold was triggered

// Pin number definations
#define LED_4 25
#define LED_3 26
#define LED_2 27
#define LED_1 28
#define LED_0 29
#define DELAY 500

#define TX_BIT0 16 
#define TX_BIT1 15 
#define TX_BIT2 14 
#define TX_BIT3 4 

#define RX_EN 18
#define RX_AIN 2

#define DFU_BUTTON 20
#define BUTTON 22

// BLE Service
BLEDis  bledis;  // device information
BLEUart bleuart; // uart over ble
BLEBas  blebas;  // battery

void setup()
{
  Serial.begin(115200);
  while ( !Serial ) delay(10);   // for nrf52840 with native usb
  
  Serial.println("Bluefruit52 BLEUART Example");
  Serial.println("---------------------------\n");

  // Setup the BLE LED to be enabled on CONNECT
  // Note: This is actually the default behaviour, but provided
  // here in case you want to control this LED manually via PIN 19
  Bluefruit.autoConnLed(true);

  // Config the peripheral connection with maximum bandwidth 
  // more SRAM required by SoftDevice
  // Note: All config***() function must be called before begin()
  Bluefruit.configPrphBandwidth(BANDWIDTH_MAX);

  Bluefruit.begin();
  // Set max power. Accepted values are: -40, -30, -20, -16, -12, -8, -4, 0, 4
  Bluefruit.setTxPower(4);
  Bluefruit.setName("AFFOA-OMS");
  //Bluefruit.setName(getMcuUniqueID()); // useful testing with multiple central connections
  Bluefruit.setConnectCallback(connect_callback);
  Bluefruit.setDisconnectCallback(disconnect_callback);

  // Configure and Start Device Information Service
  bledis.setManufacturer("Apricity Code");
  bledis.setModel("AFFOA Optical Measurement System");
  bledis.begin();

  // Configure and Start BLE Uart Service
  bleuart.begin();

  // Start BLE Battery Service
  blebas.begin();
  blebas.write(100);

  // Set up and start advertising
  startAdv();

  Serial.println("Please use Adafruit's Bluefruit LE app to connect in UART mode");
 
  //Set up Pins
  pinMode(RX_AIN, INPUT);
  pinMode(RX_EN, OUTPUT);
  pinMode(DFU_BUTTON, INPUT_PULLUP);
  pinMode(BUTTON, INPUT_PULLUP);
  pinMode(LED_0, OUTPUT);
  pinMode(LED_1, OUTPUT);
  pinMode(LED_2, OUTPUT);
  pinMode(LED_3, OUTPUT);
  pinMode(LED_4, OUTPUT);
  digitalWrite(RX_EN, HIGH);

  
  // initialize all the readings to 0 - This is for moving average:
  for (int thisReading = 0; thisReading < numReadings; thisReading++) {
    readings[thisReading] = 0;
  }
  Serial.println("Initial Setup complete");
}

void setTXPower(int level)
{
  //Take in level from 0 to 15 --- 0 is off, 15 is max
  switch (level) {
    case 0:
      digitalWrite(TX_BIT0, HIGH);
      digitalWrite(TX_BIT1, HIGH);
      digitalWrite(TX_BIT2, HIGH);
      digitalWrite(TX_BIT3, HIGH);
      break;
    case 1:
      digitalWrite(TX_BIT0, LOW);
      digitalWrite(TX_BIT1, HIGH);
      digitalWrite(TX_BIT2, HIGH);
      digitalWrite(TX_BIT3, HIGH);
      break;
    case 2:
      digitalWrite(TX_BIT0, HIGH);
      digitalWrite(TX_BIT1, LOW);
      digitalWrite(TX_BIT2, HIGH);
      digitalWrite(TX_BIT3, HIGH);
      break;
    case 3:
      digitalWrite(TX_BIT0, LOW);
      digitalWrite(TX_BIT1, LOW);
      digitalWrite(TX_BIT2, HIGH);
      digitalWrite(TX_BIT3, HIGH);
      break;
    case 4:
      digitalWrite(TX_BIT0, HIGH);
      digitalWrite(TX_BIT1, HIGH);
      digitalWrite(TX_BIT2, LOW);
      digitalWrite(TX_BIT3, HIGH);
      break;
    case 5:
      digitalWrite(TX_BIT0, LOW);
      digitalWrite(TX_BIT1, HIGH);
      digitalWrite(TX_BIT2, LOW);
      digitalWrite(TX_BIT3, HIGH);
      break;
    case 6:
      digitalWrite(TX_BIT0, HIGH);
      digitalWrite(TX_BIT1, LOW);
      digitalWrite(TX_BIT2, LOW);
      digitalWrite(TX_BIT3, HIGH);
      break;
    case 7:
      digitalWrite(TX_BIT0, LOW);
      digitalWrite(TX_BIT1, LOW);
      digitalWrite(TX_BIT2, LOW);
      digitalWrite(TX_BIT3, HIGH);
      break;
    case 8:
      digitalWrite(TX_BIT0, HIGH);
      digitalWrite(TX_BIT1, HIGH);
      digitalWrite(TX_BIT2, HIGH);
      digitalWrite(TX_BIT3, LOW);
      break;
    case 9:
      digitalWrite(TX_BIT0, LOW);
      digitalWrite(TX_BIT1, HIGH);
      digitalWrite(TX_BIT2, HIGH);
      digitalWrite(TX_BIT3, LOW);
      break;
    case 10:
      digitalWrite(TX_BIT0, HIGH);
      digitalWrite(TX_BIT1, LOW);
      digitalWrite(TX_BIT2, HIGH);
      digitalWrite(TX_BIT3, LOW);
      break;
    case 11:
      digitalWrite(TX_BIT0, LOW);
      digitalWrite(TX_BIT1, LOW);
      digitalWrite(TX_BIT2, HIGH);
      digitalWrite(TX_BIT3, LOW);
      break;
    case 12:
      digitalWrite(TX_BIT0, HIGH);
      digitalWrite(TX_BIT1, HIGH);
      digitalWrite(TX_BIT2, LOW);
      digitalWrite(TX_BIT3, LOW);
      break;
    case 13:
      digitalWrite(TX_BIT0, LOW);
      digitalWrite(TX_BIT1, HIGH);
      digitalWrite(TX_BIT2, LOW);
      digitalWrite(TX_BIT3, LOW);
      break;
    case 14:
      digitalWrite(TX_BIT0, HIGH);
      digitalWrite(TX_BIT1, LOW);
      digitalWrite(TX_BIT2, LOW);
      digitalWrite(TX_BIT3, LOW);
      break;
    case 15:
      digitalWrite(TX_BIT0, LOW);
      digitalWrite(TX_BIT1, LOW);
      digitalWrite(TX_BIT2, LOW);
      digitalWrite(TX_BIT3, LOW);
      break;
  }
}

void startAdv(void)
{
  // Advertising packet
  Bluefruit.Advertising.addFlags(BLE_GAP_ADV_FLAGS_LE_ONLY_GENERAL_DISC_MODE);
  Bluefruit.Advertising.addTxPower();

  // Include bleuart 128-bit uuid
  Bluefruit.Advertising.addService(bleuart);

  // Secondary Scan Response packet (optional)
  // Since there is no room for 'Name' in Advertising packet
  Bluefruit.ScanResponse.addName();
  
  /* Start Advertising
   * - Enable auto advertising if disconnected
   * - Interval:  fast mode = 20 ms, slow mode = 152.5 ms
   * - Timeout for fast mode is 30 seconds
   * - Start(timeout) with timeout = 0 will advertise forever (until connected)
   * 
   * For recommended advertising interval
   * https://developer.apple.com/library/content/qa/qa1931/_index.html   
   */
  Bluefruit.Advertising.restartOnDisconnect(true);
  Bluefruit.Advertising.setInterval(32, 244);    // in unit of 0.625 ms
  Bluefruit.Advertising.setFastTimeout(30);      // number of seconds in fast mode
  Bluefruit.Advertising.start(30);                // 0 = Don't stop advertising after n seconds  
}

int readRXFiber() {

  // Prevents readings at boot
  if (millis() < 200) {
    readRX = false;
  }
  
  //MOVING AVERAGE OF RX FIBER READINGS
  // subtract the last reading:
  total = total - readings[readIndex];
  // read from the sensor:
  readings[readIndex] = analogRead(RX_AIN);
  // add the reading to the total:
  total = total + readings[readIndex];
  // advance to the next position in the array:
  readIndex = readIndex + 1;

  // if we're at the end of the array...
  if (readIndex >= numReadings) {
    // ...wrap around to the beginning:
    readIndex = 0;
  }

  // calculate the average:
  average = total / numReadings;

  // Write average to Serial and BLEUart
  bleuart.println(average);
  Serial.println(average);
  
  return average;
}

void shutDown() {
  Serial.println("SHUTTING DOWN, PRESS BUTTON TO WAKE UP");
      
  digitalWrite(LED_0, LOW);
  digitalWrite(LED_1, LOW);
  digitalWrite(LED_2, LOW);
  digitalWrite(LED_3, LOW);
  digitalWrite(LED_4, LOW);
  delay(DELAY);
  digitalWrite(LED_0, HIGH);
  digitalWrite(LED_1, HIGH);
  digitalWrite(LED_2, HIGH);
  digitalWrite(LED_3, HIGH);
  digitalWrite(LED_4, HIGH);
  delay(DELAY);
  digitalWrite(LED_0, LOW);
  digitalWrite(LED_1, LOW);
  digitalWrite(LED_2, LOW);
  digitalWrite(LED_3, LOW);
  digitalWrite(LED_4, LOW);
  delay(DELAY);
  digitalWrite(LED_0, HIGH);
  digitalWrite(LED_1, HIGH);
  digitalWrite(LED_2, HIGH);
  digitalWrite(LED_3, HIGH);
  digitalWrite(LED_4, HIGH);
  delay(DELAY);
  digitalWrite(LED_0, LOW);
  digitalWrite(LED_1, LOW);
  digitalWrite(LED_2, LOW);
  digitalWrite(LED_3, LOW);
  digitalWrite(LED_4, LOW);
  delay(DELAY);
  digitalWrite(LED_0, HIGH);
  digitalWrite(LED_1, HIGH);
  digitalWrite(LED_2, HIGH);
  digitalWrite(LED_3, HIGH);
  digitalWrite(LED_4, HIGH);
  delay(DELAY);
  
  systemOff(BUTTON, LOW);
}

void loop()
{
  
  // Read the state of the button
  buttonVal = digitalRead(BUTTON);
  
  // Test for button pressed and store the down time
  if (buttonVal == LOW && buttonLast == HIGH && (millis() - btnUpTime) > long(debounce))
  {
  btnDnTime = millis();
  }
  
  // Test for button release and store the up time
  if (buttonVal == HIGH && buttonLast == LOW && (millis() - btnDnTime) > long(debounce))
  {
  if (ignoreUp == false) {
    readRX = !readRX;
    }
  else {
    ignoreUp = false;
    }
  btnUpTime = millis();
  }
  
  // Test for button held down for longer than the hold time
  if (buttonVal == LOW && (millis() - btnDnTime) > long(holdTime))
  {
  shutDown();
  ignoreUp = true;
  btnDnTime = millis();
  }
  
  buttonLast = buttonVal;

  // Read the state of the DFU button
  buttonVal2 = digitalRead(DFU_BUTTON);
  
  // Test for button pressed and store the down time
  if (buttonVal2 == LOW && buttonLast2 == HIGH && (millis() - btnUpTime2) > long(debounce))
  {
  btnDnTime2 = millis();
  }
  
  // Test for button release and store the up time
  if (buttonVal2 == HIGH && buttonLast2 == LOW && (millis() - btnDnTime2) > long(debounce))
  {
  if (ignoreUp2 == false) {
    Serial.println("DFU Button short press, does nothing yet");
    }
  else {
    ignoreUp2 = false;
    }
  btnUpTime2 = millis();
  }
  
  // Test for button held down for longer than the hold time
  if (buttonVal2 == LOW && (millis() - btnDnTime2) > long(holdTime))
  {
  Serial.println("Disconnect and start advertising");
  forceDisconnect();
  
  ignoreUp2 = true;
  btnDnTime2 = millis();
  }
  
  buttonLast2 = buttonVal2;
  
  if (readRX) {
    readRXFiber();
  }
  // Request CPU to enter low-power mode until an event/interrupt occurs
  waitForEvent();
}

// callback invoked when central connects
void connect_callback(uint16_t conn_handle)
{
  char central_name[32] = { 0 };
  Bluefruit.Gap.getPeerName(conn_handle, central_name, sizeof(central_name));

  Serial.print("Connected to ");
  Serial.println(central_name);
}

/**
 * Callback invoked when a connection is dropped
 * @param conn_handle connection where this event happens
 * @param reason is a BLE_HCI_STATUS_CODE which can be found in ble_hci.h
 * https://github.com/adafruit/Adafruit_nRF52_Arduino/blob/master/cores/nRF5/nordic/softdevice/s140_nrf52_6.1.1_API/include/ble_hci.h
 */
void disconnect_callback(uint16_t conn_handle, uint8_t reason)
{
  (void) conn_handle;
  (void) reason;

  Serial.println();
  Serial.println("Disconnected");
}

void forceDisconnect() {
  if (Bluefruit.Central.connected()) {
    Bluefruit.Central.disconnect(0);
  }
  else {
    startAdv();
  }
}
